﻿using UnityEngine;
using System.Collections;

public class LightPosition : MonoBehaviour {

	public GameObject light;  // Object that the camera tracks
	public float vSliderValue = 25.0f;  //slider

void onGUI()
		{
		vSliderValue = GUI.VerticalSlider(new Rect(100, Screen.height / 2 , 30, 250), vSliderValue, 25.0F, 10.0F);
		}	

void update()
		{
		Vector3 temp = transform.position;
		temp.y = vSliderValue;
		
		light.transform.position = temp;
		}

}
