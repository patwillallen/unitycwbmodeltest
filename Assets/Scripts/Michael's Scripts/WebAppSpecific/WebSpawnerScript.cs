﻿using UnityEngine;
using System.Collections;

public class WebSpawnerScript : MonoBehaviour {

	public int course;
	public int figure = 1;
	
	private string debug;
	public Options options;
	public Texture2D loadingIcon;
	private bool loading;
	private string url;
	//string baseUrl = Request.Url.GetLeftPart(UriPartial.Authority); 
	
	void Awake()
	{options = GameObject.FindWithTag("Options").GetComponent<Options>();}
	void Start()
	{
		Caching.CleanCache();
		loading = false;
		LoadModel(options.modelName);
	}
	public void LoadModel(string name)
	{
		course = options.courseNum;
		figure = options.figureNum;
		
		url = "http://enableeducation.com/unity/AssetBundles/Course1/G024/G024_Test";
		//url = Application.streamingAssetsPath + "/Course1/Figure1";
		print ("URL: "+url);
		//print ("url: "+url);
		//print ("Base URL: "+baseUrl);
		if (Application.isEditor)
		{
			url += ".unity3d";
		}
		else
		{
			url += ".unity3d";
		}

		StartCoroutine(ModelLoader(name));
	}
	
	IEnumerator ModelLoader(string modelName)
	{
		
		
		loading = true;
		WWW www = WWW.LoadFromCacheOrDownload(url, 1);
		
		
		
		//Wait for download to complete
		yield return www;//this is never passed, problem here.
		
		//Load and retrieve the asset bundle
		AssetBundle bundle = www.assetBundle;
	
		//Load the GameObject(s) 
		GameObject go = bundle.Load("G024_Test", typeof(GameObject)) as GameObject;
		
		//Instantiate the GameObjects
		GameObject goCopy = (GameObject)Instantiate (go) as GameObject;
		
		//Do whatever you want with the object(s)
		//goCopy.transform.parent=transform;
		if (options.touchRotate)
			goCopy.AddComponent<TouchRotate>();
		
		goCopy.transform.tag = "theModel";		
		goCopy.AddComponent<PlaceTarget>();
		goCopy.AddComponent<PanningScript>();
		
		//goCopy.AddComponent<FindFault>();
		//goCopy.AddComponent<LoadFault>();
		//goCopy.AddComponent<InputFault>();
		
		goCopy.AddComponent<HomeButton>();
		goCopy.AddComponent<ButtonRotationScript>();
		goCopy.AddComponent<ZoomScript>();
		
		goCopy.transform.localScale = new Vector3 (1f,1f,1f);
		goCopy.transform.localPosition = new Vector3 (0.0f,0.0f,0.0f);
		
		bundle.Unload(false);
		
		loading = false;
	}
	
	void OnGUI()//this is where we will create the Loading Icon
	{
		if (loading)
			GUI.DrawTexture(new Rect((Screen.width/2) - (Screen.width/24), (Screen.height/2) - (Screen.height/24), 60, 60), loadingIcon, ScaleMode.ScaleToFit, true, 10.0F);
	}
}
