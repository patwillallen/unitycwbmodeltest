﻿using UnityEngine;
using System.Collections;

public class PanningScript : MonoBehaviour {

	public GUISkin guiSkin;
	public Texture2D[] arrows;
	public GameObject options;
	
	private bool useButtons;
	private bool useRepeatButtons;
	public float degrees;
	
	private bool up;
	private bool down;
	private bool right;
	private bool left;
	
	
	void Awake()
	{
		guiSkin = Resources.Load ("ButtonSkin") as GUISkin;
		arrows = new Texture2D[4];
		arrows[0] = Resources.Load ("Materials/panUp") as Texture2D;
		arrows[1] = Resources.Load ("Materials/panDown") as Texture2D;
		arrows[2] = Resources.Load ("Materials/panLeft") as Texture2D;
		arrows[3] = Resources.Load ("Materials/panRight") as Texture2D;
		options = GameObject.FindGameObjectWithTag("Options");
	}
	
	void Start()
	{
		degrees = (options.GetComponent<Options>().rotateSensitivity);
		useButtons = options.GetComponent<Options>().buttonRotation;
		useRepeatButtons = options.GetComponent<Options>().buttonHoldRotation;
	}
	
	void OnGUI()
	{
		
		if (GUI.RepeatButton(new Rect((Screen.width/24)*21f, (Screen.height/12)*2.5f, Screen.width/24, (Screen.height/12)), arrows[2],guiSkin.button))
			left = true;
		else
			left = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/24)*23f, (Screen.height/12)*2.5f, Screen.width/24, (Screen.height/12)), arrows[3],guiSkin.button))
			right = true;
		else
			right = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/24)*22, (Screen.height/12)*2f, Screen.width/24, Screen.height/12), arrows[0],guiSkin.button))
			up = true;
		else
			up = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/24)*22, Screen.height/12*3f, Screen.width/24, Screen.height/12), arrows[1],guiSkin.button))
			down = true;
		else
			down = false;
		
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		if (useRepeatButtons)
		{
			/*Apply Movement hold down*/
			if (up)
				gameObject.transform.Translate(Vector3.down* 10.0f *Time.deltaTime, Space.World);
			if (down)
				gameObject.transform.Translate(Vector3.up* 10.0f *Time.deltaTime, Space.World);
			if (left)
				gameObject.transform.Translate(Vector3.right* 10.0f *Time.deltaTime, Space.World);
			if (right)
				gameObject.transform.Translate(Vector3.left* 10.0f *Time.deltaTime, Space.World);
		}
	}
}
