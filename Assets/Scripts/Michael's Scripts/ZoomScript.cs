﻿using UnityEngine;
using System.Collections;

public class ZoomScript : MonoBehaviour {

	public GUISkin guiSkin;
	private bool zoomIn;
	private bool zoomOut;
	
	public Texture2D [] icons;
	public float speed = 2f;
	private Vector3 origionalSize;
	private Vector3 scale;
	
	private float zoomInLimit = 2.0f;
	private float zoomOutLimit = 0.7f;
	
	void Start()
	{
		guiSkin = Resources.Load ("ButtonSkin") as GUISkin;
		icons = new Texture2D[2];
		icons[0] = Resources.Load ("Materials/ZoomIn") as Texture2D;
		icons[1] = Resources.Load ("Materials/ZoomOut") as Texture2D;
		scale = transform.localScale;
		origionalSize = new Vector3 ();
		origionalSize = scale;
	}
	
	void OnGUI()
	{
		/*Zoom In*/
		/*
		if (GUI.RepeatButton(new Rect((Screen.width/24)*23, (Screen.height/12), Screen.width/24, (Screen.height/12)), icons[0]))
		{
			
			scale = transform.localScale;
			if (scale.y < zoomInLimit)
			{
				scale.y = scale.y+((scale.y/speed)*Time.deltaTime); // your new value
				scale.x = scale.x+((scale.x/speed)*Time.deltaTime); // your new value
				scale.z = scale.z+((scale.z/speed)*Time.deltaTime); // your new value
				transform.localScale = scale;
			}
		}
		*/
		/*Zoom Out*/
		/*
		if (GUI.RepeatButton(new Rect((Screen.width/24)*21, (Screen.height/12), Screen.width/24, (Screen.height/12)), icons[1]))
		{
			scale = transform.localScale;
			if (scale.y > zoomOutLimit)
			{
				scale.y = scale.y-((scale.y/speed)*Time.deltaTime); // your new value
				scale.x = scale.x-((scale.x/speed)*Time.deltaTime); // your new value
				scale.z = scale.z-((scale.z/speed)*Time.deltaTime); // your new value
				transform.localScale = scale;
			}
		}
		*/
		//print (scale);
		
		
		if (GUI.RepeatButton(new Rect((Screen.width/24)*23, (Screen.height/12), Screen.width/24, (Screen.height/12)), icons[0],guiSkin.button))
		{
			zoomIn = true;
		}
		else
		{
			zoomIn = false;
		}
		if (GUI.RepeatButton(new Rect((Screen.width/24)*21, (Screen.height/12), Screen.width/24, (Screen.height/12)), icons[1],guiSkin.button))
		{
			zoomOut = true;
		}
		else
		{
			zoomOut = false;
		}
	}
	
	void Update()
	{
		if (zoomIn)
		{
			transform.localPosition = new Vector3 (transform.position.x,transform.position.y, transform.position.z-10*Time.deltaTime);
		}
		else if (zoomOut)
		{
			transform.localPosition = new Vector3 (transform.position.x,transform.position.y, transform.position.z+10*Time.deltaTime);
		}
	}
}
