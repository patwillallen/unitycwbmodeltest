﻿using UnityEngine;
using System.Collections;

public class ButtonRotationScript : MonoBehaviour {
	
	public GUISkin guiSkin;
	public Texture2D[] arrows;
	public GameObject options;
	
	private bool useButtons;
	private bool useRepeatButtons;
	public float degrees;
	
	private bool up;
	private bool down;
	private bool right;
	private bool left;
	
	
	void Awake()
	{
		guiSkin = Resources.Load ("ButtonSkin") as GUISkin;
		arrows = new Texture2D[4];
		arrows[0] = Resources.Load ("Materials/tiltUp") as Texture2D;
		arrows[1] = Resources.Load ("Materials/tiltDown") as Texture2D;
		arrows[2] = Resources.Load ("Materials/tiltLeft") as Texture2D;
		arrows[3] = Resources.Load ("Materials/tiltRight") as Texture2D;
		options = GameObject.FindGameObjectWithTag("Options");
	}
	
	void Start()
	{
		degrees = (options.GetComponent<Options>().rotateSensitivity);
		useButtons = options.GetComponent<Options>().buttonRotation;
		useRepeatButtons = options.GetComponent<Options>().buttonHoldRotation;
	}
	
	void OnGUI()
	{
		if (GUI.RepeatButton(new Rect((Screen.width/24)*21f, (Screen.height/12)*6, Screen.width/24, (Screen.height/12)), arrows[2],guiSkin.button))
			left = true;
		else
			left = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/24)*23f, (Screen.height/12)*6, Screen.width/24, (Screen.height/12)), arrows[3],guiSkin.button))
			right = true;
		else
			right = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/24)*22, (Screen.height/12)*5.5f, Screen.width/24, Screen.height/12), arrows[0],guiSkin.button))
			up = true;
		else
			up = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/24)*22, Screen.height/12*6.5f, Screen.width/24, Screen.height/12), arrows[1],guiSkin.button))
			down = true;
		else
			down = false;
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		
		
		if (useRepeatButtons)
		{
			/*Apply Movement hold down*/
			if (up)
				gameObject.transform.RotateAround(gameObject.transform.position, Vector3.right, degrees*Time.deltaTime);
			if (down)
				gameObject.transform.RotateAround(gameObject.transform.position, Vector3.left, degrees*Time.deltaTime);
			if (left)
				gameObject.transform.RotateAround(gameObject.transform.position, Vector3.up, degrees*Time.deltaTime);
			if (right)
				gameObject.transform.RotateAround(gameObject.transform.position, Vector3.down, degrees*Time.deltaTime);
		}
		if (useButtons)
		{
			/*Apply Movement Single Press*/
			if (up)
				transform.RotateAround(new Vector3(0,0,0) , Vector3.right, degrees);
			if (down)
				transform.RotateAround(new Vector3(0,0,0), Vector3.left, degrees);
			if (left)
				transform.RotateAround(new Vector3(0,0,0), Vector3.forward, degrees);
			if (right)
				transform.RotateAround(new Vector3(0,0,0), Vector3.back, degrees);
		}
	}
	
	void KeyCheck()
	{
		/*If Key Down*/
		if (Input.GetKeyDown(KeyCode.A))
			left = true;
		if (Input.GetKeyDown(KeyCode.D))
			right = true;
		if (Input.GetKeyDown(KeyCode.W))
			up = true;
		if (Input.GetKeyDown(KeyCode.S))
			down = true;
		/*If Key Up*/
		if (Input.GetKeyUp(KeyCode.A))
			left = false;
		if (Input.GetKeyUp(KeyCode.D))
			right = false;
		if (Input.GetKeyUp(KeyCode.W))
			up = false;
		if (Input.GetKeyUp(KeyCode.S))
			down = false;
	}
}
